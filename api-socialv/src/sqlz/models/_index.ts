import { Sequelize } from 'sequelize'

const config = {
    "development": {
        database: 'socialv_dev',
        user: 'root',
        password: '',
        object: {
          host: 'localhost',
          dialect: 'mysql',
          operatorsAliases: false,
          pool: {
            max: 5,
            min: 0
          },
          acquire: 30000,
          idle: 10000,
        }
    },
    "production": {
      database: "magus_db_microblogging",
      user: "magus",
      password: 'mazqswxedc1996__!!',
      host: "mysql-magus.alwaysdata.net",
      dialect: "mysql",
      object: {
        host: "mysql-magus.alwaysdata.net",
        dialect: "mysql",
        operatorsAliases: false,
        pool: {
          max: 5,
          min: 0
        },
        acquire: 30000,
        idle: 10000
      }
    }
};

const dbConfig = config[process.env.NODE_ENV]

const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.user,
  dbConfig.password,
  dbConfig.object
)



sequelize.authenticate().then((errors) => { console.log('errors :', errors) });

export default sequelize