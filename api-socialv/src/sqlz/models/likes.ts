import { BOOLEAN, DATE, Sequelize } from 'sequelize'
import { INTEGER } from 'sequelize'
import { TEXT } from 'sequelize'
import { RANGE } from 'sequelize'
import { Model, STRING, UUID } from 'sequelize'
import sequelize from './_index'
import * as bcrypt from "bcrypt";


export class Like extends Model {
}

// tslint:disable-next-line:max-classes-per-file
export class LikeModel {
  id: string
  like: boolean
  postId: string
  userId: string
  createdAt: Date
  updatedAt: Date
}

Like.init(
  {
    like: BOOLEAN,
    postId: INTEGER,
    userId: INTEGER
  },
  { sequelize, modelName: 'like' }
)
