import { DATE, Sequelize } from 'sequelize'
import { INTEGER } from 'sequelize'
import { TEXT } from 'sequelize'
import { RANGE } from 'sequelize'
import { Model, STRING, UUID } from 'sequelize'
import sequelize from './_index'
import * as bcrypt from "bcrypt";


export class User extends Model {
}

// tslint:disable-next-line:max-classes-per-file
export class UserModel {
  id: string
  firstname: string
  lastname: string
  username: string
  email: string
  password: string
  gender: string
  picture: string
  dateOfBirth: string
  address: string
  state: string
  listFollowers: string
  listFollowing: string
  createdAt: Date
  updatedAt: Date
}

User.init(
  {
    firstname: STRING(50),
    lastname: STRING(50),
    username: STRING(50),
    email: STRING(150),
    password: STRING(255),
    gender: STRING(10),
    picture: STRING(255),
    dateOfBirth: DATE,
    address: STRING(255),
    state: STRING(255),
    listFollowers: STRING(255),
    listFollowing: STRING(255)
  },
  { sequelize, modelName: 'user' }
)
