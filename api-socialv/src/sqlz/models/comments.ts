import { BOOLEAN, DATE, Sequelize } from 'sequelize'
import { INTEGER } from 'sequelize'
import { TEXT } from 'sequelize'
import { RANGE } from 'sequelize'
import { Model, STRING, UUID } from 'sequelize'
import sequelize from './_index'
import * as bcrypt from "bcrypt";


export class Comment extends Model {
}

// tslint:disable-next-line:max-classes-per-file
export class CommentModel {
  id: string
  content: string
  postId: string
  userId: string
  createdAt: Date
  updatedAt: Date
}

Comment.init(
  {
    content: STRING(255),
    postId: INTEGER,
    userId: INTEGER
  },
  { sequelize, modelName: 'comment' }
)
