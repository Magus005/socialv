import { DATE, Sequelize } from 'sequelize'
import { INTEGER } from 'sequelize'
import { TEXT } from 'sequelize'
import { RANGE } from 'sequelize'
import { Model, STRING, UUID } from 'sequelize'
import sequelize from './_index'
import * as bcrypt from "bcrypt";


export class Post extends Model {
}

// tslint:disable-next-line:max-classes-per-file
export class PostModel {
  id: string
  content: string
  picture: string
  link: string
  userId: string
  type: string
  createdAt: Date
  updatedAt: Date
}

Post.init(
  {
    content: STRING(250),
    picture: STRING(250),
    link: STRING(250),
    type: STRING(250),
    userId: INTEGER
  },
  { sequelize, modelName: 'post' }
)
