import { Express } from 'express'
import { PostsController } from '../api/_index'
import upload from '../utils/multer'

export function routes(app: Express) {
  app.get('/api/posts', PostsController.PostGet.list)
  app.get('/api/posts/orderBy', PostsController.PostGet.listDesc)
  app.get('/api/posts/byUser/:id', PostsController.PostGet.listByUserId)
  app.get('/api/posts/:page/:limit', PostsController.PostGet.listDesclimit)
  app.get('/api/posts/:id', PostsController.PostGet.show)
  app.post('/api/posts/:id', upload.single('picture'), PostsController.PostPost.update)
  app.post('/api/posts', upload.single('picture'), PostsController.PostPost.create)
  app.delete('/api/posts/:id', PostsController.PostDelete.remove)
}