import { Express } from 'express'
import { LikesController } from '../api/_index'

export function routes(app: Express) {

  app.get('/api/likes', LikesController.LikeGet.list)
  app.get('/api/likes/:id', LikesController.LikeGet.show)
  app.get('/api/likes/byPost/:id', LikesController.LikeGet.showByPost)
  app.get('/api/likes/byPostAndUser/:userId/:postId', LikesController.LikeGet.showByPostAndUser)
  app.post('/api/likes', LikesController.LikePost.create)
  app.post('/api/likes/:id', LikesController.LikePost.update)
  app.delete('/api/likes/:id', LikesController.LikeDelete.remove)

}