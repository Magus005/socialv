import { Express } from 'express'
import { UsersController } from '../api/_index'
import upload from '../utils/multer'

export function routes(app: Express) {

  app.get('/api/users', UsersController.UserGet.list)
  app.get('/api/users/:id', UsersController.UserGet.show)
  app.post('/api/users', upload.single('picture'), UsersController.UserPost.create)
  app.post('/api/users/:id', upload.single('picture'), UsersController.UserPost.update)
  app.post('/api/users/changePassword/:id', UsersController.UserPost.changePassword)
  app.delete('/api/users/:id', UsersController.UserDelete.remove)
  app.delete('/api/users/cancel/:id', UsersController.UserDelete.cancel)
  app.delete('/api/users/remove-all/:id', UsersController.UserDelete.removeAll)

}