import * as winston from 'winston'
import { Express, Request, Response } from 'express'
import * as UsersRoutes from './users'
import * as AuthRoutes from './auth'
import * as PostsRoutes from './posts'
import * as LikesRoutes from './likes'
import * as CommentsRoutes from './comments'

export function initRoutes(app: Express) {
  winston.log('info', '--> Initialisations des routes')

  app.get('/api', (req: Request, res: Response) => res.status(200).send({
    message: 'server is running!'
  }))

  UsersRoutes.routes(app)
  AuthRoutes.routes(app)
  PostsRoutes.routes(app)
  LikesRoutes.routes(app)
  CommentsRoutes.routes(app)

  app.all('*', (req: Request, res: Response) => res.boom.notFound())
}