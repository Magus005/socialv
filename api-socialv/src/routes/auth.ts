import { Express } from 'express'
import { AuthController } from '../api/_index'

export function routes(app: Express) {

  app.post('/login', AuthController.Auth.login)

}