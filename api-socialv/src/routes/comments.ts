import { Express } from 'express'
import { CommentsController } from '../api/_index'

export function routes(app: Express) {

  app.get('/api/comments', CommentsController.CommentGet.list)
  app.get('/api/comments/:id', CommentsController.CommentGet.show)
  app.get('/api/comments/byPost/:id', CommentsController.CommentGet.showByPost)
  app.get('/api/comments/byPostAndUser/:userId/:postId', CommentsController.CommentGet.showByPostAndUser)
  app.post('/api/comments', CommentsController.CommentPost.create)
  app.post('/api/comments/:id', CommentsController.CommentPost.update)
  app.delete('/api/comments/:id', CommentsController.CommentDelete.remove)

}