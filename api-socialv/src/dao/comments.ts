import * as uuid from 'uuid'
import { Comment } from './../sqlz/models/comments'

export function create(comment: any): Promise<any> {
  return Comment
    .create({
      content: comment.content,
      userId: comment.userId,
      postId: comment.postId
    })
}
export function update(comment: any, id): Promise<any> {
    return Comment
        .update(comment, {where: {id}})
}

export function findAll(): Promise<any> {
  return Comment
    .findAll()
}

export function findOne(id): Promise<any> {
    return Comment.findOne({where: {id}})
}

export function findByComment(id): Promise<any> {
    return Comment.findAll({where: {postId: id}})
}

export function findByCommentAndUser(postId, userId): Promise<any> {
    return Comment.findOne({where: {postId, userId}})
}

export function destroy(id): Promise<any> {
    return Comment.destroy({where: {id}})
}

export function destroyByPost(id): Promise<any> {
    return Comment.destroy({where: {postId: id}})
}

export function destroyByUser(id): Promise<any> {
    return Comment.destroy({where: {userId: id}})
}
