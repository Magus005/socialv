import { User } from './../sqlz/models/users'

export function create(user: any): Promise<any> {
  return User
    .create({
      firstname: user.firstname,
      lastname: user.lastname,
      username: user.username,
      email: user.email,
      password: user.password,
      gender: user.gender,
      picture: user.picture,
      dateOfBirth: user.dateOfBirth,
      address: user.address,
      state: user.state,
      listFollowers: user.listFollowers,
      listFollowing: user.listFollowing
    })
}


export function update(user: any, id): Promise<any> {
    return User
        .update(user, {where: {id}})
}

export function findAll(): Promise<any> {
  return User
    .findAll()
}

export function findOne(id): Promise<any> {
    console.log('id', id)
    return User.findOne({where: {id}})
}

export function destroy(id): Promise<any> {
    return User.destroy({where: {id}})
}

export function removeFollowingAndFollower(userId, userIdToBeDeleted): Promise<any> {
  const user = User.findOne({ where: { id: userId }});
  const indexFollower = user['listFollowers'].indexOf(userIdToBeDeleted);
  const indexFollowing = user['listFollowing'].indexOf(userIdToBeDeleted);

  if (indexFollower > -1) {
    user['listFollowers'].splice(indexFollower, 1);
  }
  if (indexFollowing > -1) {
    user['listFollowers'].splice(indexFollowing, 1);
  }
  return User.update(user, {where: {userId}})
}
