import { Post } from './../sqlz/models/posts'

export function create(post: any): Promise<any> {
  return Post
    .create({
      content: post.content,
      picture: post.picture,
      link: post.link,
      userId: post.userId,
      type: post.type
    })
}
export function update(post: any, id): Promise<any> {
    return Post
        .update(post, {where: {id}})
}

export function findAll(): Promise<any> {
  return Post
    .findAll()
}

export function findAllByUserId(id): Promise<any> {
  return Post.findAll({ where: {userId: id}});
}

export function findAllOrderDesc(field): Promise<any> {
  return Post
    .findAll({ order: [['updatedAt', 'DESC']]})
}

export function findAllOrderDescLimit(field, limit, offset): Promise<any> {
  return Post
    .findAll({ order: [[`${field}`, 'DESC']], offset: Number(offset), limit: Number(limit)})
}

export function findOne(id): Promise<any> {
    return Post.findOne({where: {id}})
}

export function destroy(id): Promise<any> {
    return Post.destroy({where: {id}})
}

export function destroyByUser(id): Promise<any> {
    return Post.destroy({where: {userId: id}})
}

