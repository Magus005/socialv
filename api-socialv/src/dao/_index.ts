import * as UsersDao from './users'
import * as PostsDao from './posts'
import * as LikesDao from './likes'
import * as CommentsDao from './comments'

export { UsersDao, PostsDao, LikesDao, CommentsDao }