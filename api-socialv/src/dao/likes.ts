import * as uuid from 'uuid'
import { Like } from './../sqlz/models/likes'

export function create(like: any): Promise<any> {
  return Like
    .create({
      like: like.like,
      userId: like.userId,
      postId: like.postId
    })
}
export function update(like: any, id): Promise<any> {
    return Like
        .update(like, {where: {id}})
}

export function findAll(): Promise<any> {
  return Like
    .findAll()
}

export function findOne(id): Promise<any> {
    return Like.findOne({where: {id}})
}

export function findByPost(id): Promise<any> {
    return Like.findAll({where: {postId: id}})
}

export function findByPostAndUser(postId, userId): Promise<any> {
    return Like.findOne({where: {postId, userId}})
}

export function destroy(id): Promise<any> {
    return Like.destroy({where: {id}})
}

export function destroyByPost(id): Promise<any> {
  return Like.destroy({where: {postId: id}})
}

export function destroyByUser(id): Promise<any> {
  return Like.destroy({where: {userId: id}})
}
