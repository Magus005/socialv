import * as UsersController from './users/_index'
import * as AuthController from './auth/_index'
import * as PostsController from './posts/_index'
import * as LikesController from './likes/_index'
import * as CommentsController from './comments/_index'

export { UsersController, AuthController, PostsController, LikesController, CommentsController }