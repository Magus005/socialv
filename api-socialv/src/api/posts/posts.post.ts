import { Request, Response } from 'express'
import { PostsDao } from '../../dao/_index'


const pathFile = 'http://localhost:5000';

interface MulterRequest extends Request {
    file: any;
}

export function create(req: Request, res: Response) {
    const documentFile  = (req as MulterRequest).file;
    let picture = null;
    if(documentFile) {
        picture = `${pathFile}/${documentFile.filename}`
    }
    const POST = {
        content: req.body.content,
        picture,
        link: req.body.link ? req.body.link : null,
        userId: req.body.userId,
        type: "Post"
    }
    return PostsDao.create(POST)
        .then(post => res.status(201).send(post))
        .catch(error => res.boom.badRequest(error))
}

export async function update(req: Request, res: Response) {
    const documentFile  = (req as MulterRequest).file;
    const { id } = req.params;
    const postToUpdate = await PostsDao.findOne(id);
    if(!postToUpdate) {
        return res.status(204).json(`no content`);
    }
    if(documentFile) {
        const picture = `${pathFile}/${documentFile.filename}`
        req.body.picture = picture;
    }
    return PostsDao.update(req.body, id)
        .then(post => res.status(201).send(post))
        .catch(error => res.boom.badRequest(error))
}