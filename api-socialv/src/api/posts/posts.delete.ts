import { Request, Response } from 'express'
import { PostsDao } from '../../dao/_index'

export function remove(req: Request, res: Response) {
    const { id } = req.params;
  return PostsDao
    .destroy(id)
    .then(post => res.sendStatus(200).send(post))
    .catch(error => res.boom.badRequest(error))
}
