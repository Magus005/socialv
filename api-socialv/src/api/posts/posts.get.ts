import { Request, Response } from 'express'
import { PostsDao } from '../../dao/_index'

export function list(req: Request, res: Response) {
  return PostsDao
    .findAll()
    .then(posts => res.status(200).send(posts))
    .catch(error => res.boom.badRequest(error))
}

export function listDesc(req: Request, res: Response) {
  return PostsDao
    .findAllOrderDesc('updatedAt')
    .then(posts => res.status(200).send(posts))
    .catch(error => res.boom.badRequest(error))
}

export function listByUserId(req: Request, res: Response) {
  const { id } = req.params;
  return PostsDao
    .findAllByUserId(id)
    .then(posts => res.status(200).send(posts))
    .catch(error => res.boom.badRequest(error))
}

export function listDesclimit(req: Request, res: Response) {
  const { page, limit } = req.params;
  const offset = Number(page) * Number(limit);
  return PostsDao
    .findAllOrderDescLimit('updatedAt', limit, (Number(page) * Number(limit)))
    .then(posts => res.status(200).send(posts))
    .catch(error => res.boom.badRequest(error))
}

export function show(req: Request, res: Response) {
    return PostsDao
        .findOne(req.params.id)
        .then(post => res.status(200).send(post))
        .catch(error => res.boom.badRequest(error))
}