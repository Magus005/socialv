import * as PostPost from './posts.post'
import * as PostGet from './posts.get'
import * as PostDelete from './posts.delete'

export { PostPost, PostGet, PostDelete }