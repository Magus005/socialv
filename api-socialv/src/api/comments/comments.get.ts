import { Request, Response } from 'express'
import { CommentsDao, UsersDao } from '../../dao/_index'

export function list(req: Request, res: Response) {
  return CommentsDao
    .findAll()
    .then(comments => res.status(200).send(comments))
    .catch(error => res.boom.badRequest(error))
}

export function show(req: Request, res: Response) {
    return CommentsDao
        .findOne(req.params.id)
        .then(comment => res.status(200).send(comment))
        .catch(error => res.boom.badRequest(error))
}

export function showByPost(req: Request, res: Response) {
    const listComments = [];
    return CommentsDao
        .findByComment(req.params.id)
        .then(comments => {
            comments.forEach(comment => {
                UsersDao.findOne(comment.userId).then(user => {
                    listComments.push({comment, user});
                    if(listComments.length === comments.length) {
                        res.status(200).send(listComments)
                    }
                })
                .catch(error => res.boom.badRequest(error));
            });
        })
        .catch(error => res.boom.badRequest(error))
}

export function showByPostAndUser(req: Request, res: Response) {
  const { userId, postId } = req.params;
    return CommentsDao
        .findByCommentAndUser(postId, userId)
        .then(comment => res.status(200).send(comment))
        .catch(error => res.boom.badRequest(error))
}
