import { Request, Response } from 'express'
import { CommentsDao } from '../../dao/_index'

export function remove(req: Request, res: Response) {
  const { id } = req.params;
  return CommentsDao
    .destroy(id)
    .then(comment => res.status(200).send(comment))
    .catch(error => res.boom.badRequest(error))
}
