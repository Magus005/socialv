import { Request, Response } from 'express'
import { CommentsDao } from '../../dao/_index'

export function create(req: Request, res: Response) {
    return CommentsDao.create(req.body)
        .then(comment => res.status(201).send(comment))
        .catch(error => res.boom.badRequest(error))
}


export async function update(req: Request, res: Response) {
    const { id } = req.params;
    const commentToUpdate = await CommentsDao.findOne(req.params.id);
    if(!commentToUpdate) {
        return res.status(204).json(`no content`);
    }
    return CommentsDao.update(req.body, id)
        .then(comment => res.status(201).send(comment))
        .catch(error => res.boom.badRequest(error))
}

