import * as CommentGet from './comments.get'
import * as CommentPost from './comments.post'
import * as CommentDelete from './comments.delete'

export { CommentGet, CommentPost, CommentDelete }