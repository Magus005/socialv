import { Request, Response } from 'express'
import { User } from '../../sqlz/models/users';
import { signToken } from '../../auth/passportHandler'
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';

const JWT_SECRET = 'socialv-dev';

export function login(req: Request, res: Response) {
  const { email, password } = req.body;
  User.findOne({where: { email, state: 'active' }})
    .then((user) => {
      if (!user) {
        return res.status(401).send({ message: 'Authentication failed. User not found.' });
      }
      generateToken(user, password, res)
    })
    .catch((error) => res.status(400).send(error));
}

async function generateToken(user, password, res) {
  bcrypt.compare(password, user.password, (err, isMatch) => {
    if(isMatch && !err) {
      const token = signToken(user.id, user.role);
      jwt.verify(token, JWT_SECRET, (error, data) => {
        console.log(error, data);
      })
      res.json({success: true, token: 'Bearer Token ' + token, user});
    } else {
      res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
    }
  });
}