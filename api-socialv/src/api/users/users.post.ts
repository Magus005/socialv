import { Request, Response } from 'express'
import { UsersDao } from '../../dao/_index'
import * as bcrypt from 'bcrypt';

const pathFile = 'http://localhost:5000';

interface MulterRequest extends Request {
    file: any;
}

export function create(req: Request, res: Response) {
    const documentFile  = (req as MulterRequest).file;
    console.log('req.body', req.body);
    console.log('req.file', documentFile);
    var picture = null;
    if(documentFile) {
        picture = `${pathFile}/${documentFile.filename}`;
    }
    req.body.picture = picture;
    const USER = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        username: req.body.username,
        email: req.body.email,
        password: hashPassword(req.body.password),
        gender: req.body.gender,
        picture,
        dateOfBirth: req.body.dateOfBirth,
        address: req.body.address,
        state: 'active',
        listFollowers: null,
        listFollowing: null
    }
    return UsersDao.create(USER)
        .then(user => res.status(201).send(user))
        .catch(error => res.boom.badRequest(error))
}


export async function update(req: Request, res: Response) {
    const documentFile  = (req as MulterRequest).file;
    const { id } = req.params;
    const userToUpdate = await UsersDao.findOne(id);
    if(!userToUpdate) {
        return res.status(204).json(`no content`);
    }
    if(documentFile) {
        const picture = `${pathFile}/${documentFile.filename}`
        req.body.picture = picture;
    }
    if(req.body.password) {
        req.body.password = hashPassword(req.body.password);
    }
    return UsersDao.update(req.body, id)
        .then(user => res.status(201).send(user))
        .catch(error => res.boom.badRequest(error))
}

export async function changePassword(req: Request, res: Response) {
    const { userId, currentPassword, newPassword, verifyPassword } = req.body;
    const userToUpdate = await UsersDao.findOne(userId);
    if(!userToUpdate) {
        return res.status(204).json(`no content`);
    }
    bcrypt.compare(currentPassword, userToUpdate.password, (err, isMatch) => {
        if (isMatch && !err) {
            if(newPassword == verifyPassword) {
                return UsersDao.update({password: newPassword}, userId)
                    .then(user => res.status(201).send(user))
                    .catch(error => res.boom.badRequest(error))
            } else {
                res.status(401).send({ success: false, msg: 'the new password and the verification password are different' });
            }
        }
        else {
            res.status(401).send({ success: false, msg: 'Password incorrect.' });
        }
    });

}

function hashPassword(password) {
    return bcrypt.hashSync(password, 16);
}
