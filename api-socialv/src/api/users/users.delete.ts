import { Request, Response } from 'express'
import { CommentsDao, LikesDao, PostsDao, UsersDao } from '../../dao/_index'

export function remove(req: Request, res: Response) {
  const { id } = req.params;
  return UsersDao
    .destroy(id)
    .then(user => res.status(200).send(user))
    .catch(error => res.boom.badRequest(error))
}


export function cancel(req: Request, res: Response) {
  const { id } = req.params;
  return UsersDao
    .update({state: 'inactive'}, id)
    .then(user => res.status(200).send(user))
    .catch(error => res.boom.badRequest(error))
}

export function removeAll(req: Request, res: Response) {
  const { id } = req.params;
  const userId = id;
  return PostsDao.findAllByUserId(userId)
    .then(posts => {
      const verifPost = [];
      posts.forEach(post => {
        CommentsDao.destroyByPost(post.id);
        LikesDao.destroyByPost(post.id);
        verifPost.push(1);
        if (posts.length === verifPost.length) {
          PostsDao.destroyByUser(userId);
          LikesDao.destroyByUser(userId);

          UsersDao.findOne(userId)
            .then(user => {
              const verifFollow = [];
              if(user['listFollowers'] != null) {
                user['listFollowers'].toString().split(',').foreach((id) => {
                  UsersDao.removeFollowingAndFollower(id, user.id);
                  verifFollow.push(1);
                })
              }
              if(user['listFollowers'] != null) {
                user['listFollowing'].toString().split(',').foreach((id) => {
                  UsersDao.removeFollowingAndFollower(id, user.id);
                })
              }
              PostsDao.destroyByUser(userId);
              if (verifFollow.length == user['listFollowers'].toString().split(',').length) {
                return UsersDao.destroy(userId)
                  .then(x => res.status(200).send(x))
                  .catch(error => res.boom.badRequest(error))
              }
            })
            .catch(error => res.boom.badRequest(error))
        }
      });
    })
    .catch(error => res.boom.badRequest(error))
}