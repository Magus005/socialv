import * as UserGet from './users.get'
import * as UserPost from './users.post'
import * as UserDelete from './users.delete'

export { UserGet, UserPost, UserDelete }