import { Request, Response } from 'express'
import { UsersDao } from '../../dao/_index'

export function list(req: Request, res: Response) {
  return UsersDao
    .findAll()
    .then(users => res.status(200).send(users))
    .catch(error => res.boom.badRequest(error))
}

export function show(req: Request, res: Response) {
    return UsersDao
        .findOne(req.params.id)
        .then(user => res.status(200).send(user))
        .catch(error => res.boom.badRequest(error))
}