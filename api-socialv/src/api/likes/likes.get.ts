import { Request, Response } from 'express'
import { LikesDao } from '../../dao/_index'

export function list(req: Request, res: Response) {
  return LikesDao
    .findAll()
    .then(likes => res.status(200).send(likes))
    .catch(error => res.boom.badRequest(error))
}

export function show(req: Request, res: Response) {
    return LikesDao
        .findOne(req.params.id)
        .then(like => res.status(200).send(like))
        .catch(error => res.boom.badRequest(error))
}

export function showByPost(req: Request, res: Response) {
    return LikesDao
        .findByPost(req.params.id)
        .then(like => res.status(200).send(like))
        .catch(error => res.boom.badRequest(error))
}

export function showByPostAndUser(req: Request, res: Response) {
  const { userId, postId } = req.params;
    return LikesDao
        .findByPostAndUser(postId, userId)
        .then(like => res.status(200).send(like))
        .catch(error => res.boom.badRequest(error))
}
