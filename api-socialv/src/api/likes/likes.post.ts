import { Request, Response } from 'express'
import { LikesDao } from '../../dao/_index'

export function create(req: Request, res: Response) {
    return LikesDao.create(req.body)
        .then(like => res.status(201).send(like))
        .catch(error => res.boom.badRequest(error))
}


export async function update(req: Request, res: Response) {
    const { id } = req.params;
    const likeToUpdate = await LikesDao.findOne(req.params.id);
    if(!likeToUpdate) {
        return res.status(204).json(`no content`);
    }
    return LikesDao.update(req.body, id)
        .then(like => res.status(201).send(like))
        .catch(error => res.boom.badRequest(error))
}

