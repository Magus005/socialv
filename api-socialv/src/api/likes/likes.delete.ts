import { Request, Response } from 'express'
import { LikesDao } from '../../dao/_index'

export function remove(req: Request, res: Response) {
    const { id } = req.params;
  return LikesDao
    .destroy(id)
    .then(like => res.status(200).send(like))
    .catch(error => res.boom.badRequest(error))
}
