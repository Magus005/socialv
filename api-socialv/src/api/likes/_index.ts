import * as LikeGet from './likes.get'
import * as LikePost from './likes.post'
import * as LikeDelete from './likes.delete'

export { LikeGet, LikePost, LikeDelete }