import * as jwt from 'jsonwebtoken';
import * as expressJwt from 'express-jwt';
import compose from 'composable-middleware';
import { User } from '../sqlz/models/users';

const JWT_SECRET = 'socialv-dev'

const validateJwt = expressJwt({ secret: JWT_SECRET, algorithms: ['HS256'] });

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
export function isAuthenticated() {
  return compose()
    // Validate jwt
    .use((req, res, next) => {
      verifJwt(req, res, next);
    })
    // Attach user to request
    .use((req, res, next) => {
      getUserById(req, res, next);
    });
}

function verifJwt(req, res, next) {
  // allow access_token to be passed through query parameter as well
  if(req.query && req.query.hasOwnProperty('access_token')) {
    req.headers.authorization = `Bearer ${req.query.access_token}`;
  }
  // IE11 forgets to set Authorization header sometimes. Pull from cookie instead.
  if(req.query && typeof req.headers.authorization === 'undefined') {
    req.headers.authorization = `Bearer ${req.cookies.token}`;
  }
  validateJwt(req, res, next);
}

function getUserById(req, res, next) {
  return User.findOne({ where: { id: req.user.id}})
    .then(user => {
      if(!user) {
        return res.status(401).end();
      }
      req.user = user;
      next();
      return null;
    })
    .catch(err => next(err));
}


/**
 * Returns a jwt token signed by the app secret
 */
export function signToken(id, role) {
  return jwt.sign({ _id: id, role }, JWT_SECRET, {
    expiresIn: 60 * 60
  });
}