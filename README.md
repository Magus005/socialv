# Microblogging

Service de microblogging du même type que Twitter.

![caption](intro.mp4)

## Projets
### Back-office: Angular
- ng serve
### API: NodeJS Express SequelizeORM MySql 
- npm run start:dev
- migration base de donnée: $ sequelize-cli db:migrate

## Contact
### Madiara Gassama <madiara.gassama005@gmail.com>
### Madiara Gassama <gassama.madiara@gmail.com>