import { Routes } from '@angular/router';
import { LoginComponent } from './login.component';


export const LoginRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: LoginComponent }
];
