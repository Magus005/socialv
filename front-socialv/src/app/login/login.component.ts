import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ThemeService } from '../shared/services/theme/theme.service';
import { UsersService } from '../shared/services/users/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';
  isThemeDark: Observable<boolean>;
  dynamicFlag = false;
  alertDanger = false;
  alertDangerMessage = '';


  constructor(private themeService: ThemeService, @Inject(DOCUMENT) private document: Document,
              private userService: UsersService, public router: Router) { }

  ngOnInit(): void {
    this.isThemeDark = this.themeService.isThemeDark;
    if (!localStorage.getItem('theme')) {
      localStorage.setItem('theme', '0');
    } else {
      this.loadCssDynamically(localStorage.getItem('theme'));
    }
  }




  clickDarkTheme(event) {
    event.preventDefault();

    if (localStorage.getItem('theme') === '1') {
      localStorage.setItem('theme', '0');
    } else {
      localStorage.setItem('theme', '1');
    }
    this.loadCssDynamically(localStorage.getItem('theme'));
  }

  loadCssDynamically(value) {
    if (value === '0') { // light
      this.loadTheme('/assets/css/typography.css');
      this.loadTheme('/assets/css/style.css');
    } else {
      this.loadTheme('/assets/dark/css/typography.css');
      this.loadTheme('/assets/dark/css/style.css');
    }
    this.dynamicFlag = true;
  }

  loadTheme(cssFile: string) {
    const headEl = this.document.getElementsByTagName('head')[0];
    const newLinkEl = this.document.createElement('link');
    newLinkEl.rel = 'stylesheet';
    newLinkEl.href = cssFile;

    headEl.appendChild(newLinkEl);
  }


  signIn(event) {
    event.preventDefault();
    this.alertDanger = false;
    console.log('email', this.email);
    console.log('password', this.password);

    const login = {
      email: this.email,
      password: this.password
    };

    this.userService.login(login).subscribe((response: {}) => {
      console.log('response user: ', response['user']);
      localStorage.setItem('access_token', response['token']);
      localStorage.setItem('current_user', JSON.stringify(response['user']));
      console.log('current user', JSON.parse(localStorage.getItem('current_user')).id);
      console.log('current user', localStorage.getItem('current_user'));
      console.log('current user', localStorage.getItem('access_token'));
      this.router.navigate(['/']);
    }, (error) => {
      this.alertDanger = true;
      this.alertDangerMessage = error.message;
      console.log('error: ', error);
    });
  }

}
