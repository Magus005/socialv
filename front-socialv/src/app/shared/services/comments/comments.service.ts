import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API creata() method => Create Comment
  create(comment): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/comments`, comment);
  }

  // HttpClient API creata() method => Update Comment
  update(comment, id): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/comments/${id}`, comment);
  }

  // HttpClient API creata() method => List Comment by Post
  findById(id): Observable<any> {
    return this.http.get(`${environment.apiUri}/api/comments/${id}`);
  }

  // HttpClient API creata() method => List Comment by Post
  findByPost(id): Observable<any> {
    return this.http.get(`${environment.apiUri}/api/comments/byPost/${id}`);
  }

  // HttpClient API creata() method => List Comment by Post
  findByPostAndUser(userId, postId): Observable<any> {
    return this.http.get(`${environment.apiUri}/api/comments/byPostAndUser/${userId}/${postId}`);
  }
}
