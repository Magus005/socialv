import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {


  private themeDark: Subject<boolean> = new Subject<boolean>();

  isThemeDark = this.themeDark.asObservable();

  constructor() { }

  setDarkTheme(isThemeDark: boolean) {
    this.themeDark.next(isThemeDark);

    if (isThemeDark === true) {
      console.log('Dark Used');
      localStorage.setItem('dark', 'true');
    }
    else {
      console.log('Light Used');
      localStorage.setItem('dark', 'false');
    }
  }

}
