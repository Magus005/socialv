import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API login() method => login
  login(login): Observable<any> {
    return this.http.post(`${environment.apiUri}/login`, login);
  }

  // HttpClient API creata() method => Create User
  create(user): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/users`, user);
  }

  // HttpClient API update() method => Update User
  update(user, id): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/users/${id}`, user);
  }

  // HttpClient API changePassword() method => Change Password User
  changePassword(user, id): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/users/changePassword/${id}`, user);
  }

  // HttpClient API findById() method => Find One User By ID
  findById(id): Observable<any> {
    return this.http.get(`${environment.apiUri}/api/users/${id}`);
  }

  // HttpClient API cancel() method => Cancel User By ID
  cancel(id): Observable<any> {
    return this.http.delete(`${environment.apiUri}/api/users/cancel/${id}`);
  }

  // HttpClient API delete() method => Delete User By ID
  hardDelete(id): Observable<any> {
    return this.http.delete(`${environment.apiUri}/api/users/remove-all/${id}`);
  }


  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
