import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API post() method => Create post
  create(post): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/posts/`, post);
  }

  // HttpClient API update() method => Update post
  update(post, id): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/posts/${id}`, post);
  }

  // HttpClient API remove() method => Delete post
  remove(id): Observable<any> {
    return this.http.delete(`${environment.apiUri}/api/posts/${id}`);
  }


  // HttpClient API get() method => List posts
  find(): Observable<any> {
    return this.http.get<any>(`${environment.apiUri}/api/posts/`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // HttpClient API get() method => List posts
  findByUser(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUri}/api/posts/byUser/${id}`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // HttpClient API get() method => List posts order by updatedAt Desc
  findOrderBy(): Observable<any> {
    return this.http.get<any>(`${environment.apiUri}/api/posts/orderBy`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // HttpClient API get() method => List posts order by updatedAt Desc
  findByStep(page, limit): Observable<any> {
    return this.http.get<any>(`${environment.apiUri}/api/posts/${page}/${limit}`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }



  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

