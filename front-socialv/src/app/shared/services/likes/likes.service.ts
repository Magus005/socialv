import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LikesService {


  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API creata() method => Create Like
  create(like): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/likes`, like);
  }

  // HttpClient API creata() method => Update Like
  update(like, id): Observable<any> {
    return this.http.post(`${environment.apiUri}/api/likes/${id}`, like);
  }

  // HttpClient API creata() method => List Like by Post
  findById(id): Observable<any> {
    return this.http.get(`${environment.apiUri}/api/likes/${id}`);
  }

  // HttpClient API creata() method => List Like by Post
  findByPost(id): Observable<any> {
    return this.http.get(`${environment.apiUri}/api/likes/byPost/${id}`);
  }

  // HttpClient API creata() method => List Like by Post
  findByPostAndUser(userId, postId): Observable<any> {
    return this.http.get(`${environment.apiUri}/api/likes/byPostAndUser/${userId}/${postId}`);
  }

}
