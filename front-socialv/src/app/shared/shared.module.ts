import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from 'src/components/header/header.component';
import { FooterComponent } from 'src/components/footer/footer.component';
import { CardViewImageComponent } from 'src/components/card-view-image/card-view-image.component';
import { CardViewImageMultiComponent } from 'src/components/card-view-image-multi/card-view-image-multi.component';
import { CardViewTextComponent } from 'src/components/card-view-text/card-view-text.component';
import { CardViewUriComponent } from 'src/components/card-view-uri/card-view-uri.component';
import { NgxLinkPreviewModule } from 'ngx-link-preview';
import { CardViewComponent } from 'src/components/card-view/card-view.component';
import { CommentItemComponent } from 'src/components/comment-item/comment-item.component';
@NgModule({
  exports: [
      CommonModule,
      HeaderComponent,
      FooterComponent,
      CardViewImageComponent,
      CardViewImageMultiComponent,
      CardViewTextComponent,
      CardViewUriComponent,
      CardViewComponent,
      CommentItemComponent
  ],
  imports: [
      RouterModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      NgxLinkPreviewModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    CardViewImageComponent,
    CardViewImageMultiComponent,
    CardViewTextComponent,
    CardViewUriComponent,
    CardViewComponent,
    CommentItemComponent
  ]
})
export class SharedModule { }
