import { Routes } from '@angular/router';
import { AppComponent } from './app.component';

export const AppRoutes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: ''
  },
  {
    path: '',
    loadChildren: () => import('src/app/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'login',
    loadChildren: () => import('src/app/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'register',
    loadChildren: () => import('src/app/register/register.module').then(m => m.RegisterModule)
  },
  {
    path: 'privacy-setting',
    loadChildren: () => import('src/app/privacy-setting/privacy-setting.module').then(m => m.PrivacySettingModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('src/app/my-profile/my-profile.module').then(m => m.MyProfileModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('src/app/edit-profile/edit-profile.module').then(m => m.EditProfileModule)
  },

];
