import { Routes } from '@angular/router';
import { AuthGuardService as AuthGuard  } from '../auth/auth-guard.service';
import { EditProfileComponent } from './edit-profile.component';


export const EditProfileRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: EditProfileComponent, canActivate: [AuthGuard]  }
];
