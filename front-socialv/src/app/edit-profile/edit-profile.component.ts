import { Component, OnInit } from '@angular/core';
import { UsersService } from '../shared/services/users/users.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  currentUser: any = {};

  currentPassword = '';
  newPassword = '';
  verifyPassword = '';

  firstname = '';
  lastname = '';
  username = '';
  address = '';
  email = '';

  alertDangerMessage = '';
  alertSuccessMessage = '';

  alertDangerMessage_1 = '';
  alertSuccessMessage_1 = '';
  alertDanger = false;
  alertSuccess = false;

  alertDanger_1 = false;
  alertSuccess_1 = false;

  constructor(private userService: UsersService) { }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('current_user'));
    this.firstname = this.currentUser.firstname;
    this.lastname = this.currentUser.lastname;
    this.username = this.currentUser.username;
    this.address = this.currentUser.address;
    this.email = this.currentUser.email;
  }

  changePassword(event) {
    event.preventDefault();
    this.alertSuccess = false;
    this.alertDanger = false;
    const pass = {
      userId: this.currentUser.id,
      currentPassword: this.currentPassword,
      newPassword: this.newPassword,
      verifyPassword: this.verifyPassword
    };
    this.userService.changePassword(pass, this.currentUser.id).subscribe((response: any) => {
      console.log('response : ', response);
      this.alertSuccess = true;
      this.alertSuccessMessage = 'password changed successfully';
      this.newPassword = '';
      this.currentPassword = '';
      this.verifyPassword = '';
    }, (error) => {
      this.alertDanger = true;
      this.alertDangerMessage = error.error.msg;
      console.log('error ', error);
    })
  }

  editProfile(event) {
    event.preventDefault();
    this.alertSuccess_1 = false;
    this.alertDanger_1 = false;
    const user = {
      firstname: this.firstname,
      lastname: this.lastname,
      username: this.username,
      address: this.address,
      email: this.email
    };

    this.userService.update(user, this.currentUser.id).subscribe((response: any) => {

      this.alertDangerMessage_1 = 'profile updated successfully';
      this.alertSuccess_1 = true;
      this.newPassword = '';
      this.currentPassword = '';
      this.verifyPassword = '';
    }, (error) => {
      this.alertDanger_1 = true;
      this.alertDangerMessage_1 = error.error.msg;
      console.log('error ', error);
    })
  }

}
