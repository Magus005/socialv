import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ThemeService } from '../shared/services/theme/theme.service';
import { UsersService } from '../shared/services/users/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  email = '';
  password = '';
  address = '';
  dateOfBirth = '';
  gender = 'M';
  genderM = false;
  genderF = true;
  username = '';
  lastname = '';
  firstname = '';


  isThemeDark: Observable<boolean>;
  dynamicFlag = false;
  alertDanger = false;
  alertDangerMessage = '';
  message = '';
  url: any = null;
  imagePath: any = null;


  constructor(private themeService: ThemeService, @Inject(DOCUMENT) private document: Document,
              private userService: UsersService, public router: Router) { }

  ngOnInit(): void {
    this.isThemeDark = this.themeService.isThemeDark;
    if (!localStorage.getItem('theme')) {
      localStorage.setItem('theme', '0');
    } else {
      this.loadCssDynamically(localStorage.getItem('theme'));
    }
  }




  clickDarkTheme(event) {
    event.preventDefault();

    if (localStorage.getItem('theme') === '1') {
      localStorage.setItem('theme', '0');
    } else {
      localStorage.setItem('theme', '1');
    }
    this.loadCssDynamically(localStorage.getItem('theme'));
  }

  loadCssDynamically(value) {
    if (value === '0') { // light
      this.loadTheme('/assets/css/typography.css');
      this.loadTheme('/assets/css/style.css');
    } else {
      this.loadTheme('/assets/dark/css/typography.css');
      this.loadTheme('/assets/dark/css/style.css');
    }
    this.dynamicFlag = true;
  }

  loadTheme(cssFile: string) {
    const headEl = this.document.getElementsByTagName('head')[0];
    const newLinkEl = this.document.createElement('link');
    newLinkEl.rel = 'stylesheet';
    newLinkEl.href = cssFile;

    headEl.appendChild(newLinkEl);
  }


  onFileChanged(event) {
    const files = event.target.files;
    if (files.length === 0) { return; }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
        this.message = "Only images are supported.";
        return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
        this.url = reader.result;
    };
  }

  signUp(event) {
    event.preventDefault();

    const register: any = new FormData();
    register.append('email', this.email);
    register.append('password', this.password);
    register.append('address', this.address);
    register.append('dateOfBirth', this.dateOfBirth);
    if (this.imagePath != null) {
      register.append('picture', this.imagePath[0]);
    }
    register.append('gender', this.gender);
    register.append('username', this.username);
    register.append('lastname', this.lastname);
    register.append('firstname', this.firstname);



    this.userService.create(register).subscribe((response: {}) => {
      console.log(response);
      this.login(this.email, this.password);
    }, (error) => {
      this.alertDanger = true;
      this.alertDangerMessage = error.message;
      console.log('error: ', error);
    });
  }

  login(email, password) {
    this.userService.login({email, password}).subscribe((response: {}) => {
      localStorage.setItem('access_token', response['token']);
      localStorage.setItem('current_user', JSON.stringify(response['user']));
      this.router.navigate(['/']);
    }, (error) => {
      this.alertDanger = true;
      this.alertDangerMessage = error.message;
      console.log('error: ', error);
    });
  }

  changeItemGender(value, event) {
    event.preventDefault();
    this.gender = value;
  }

}
