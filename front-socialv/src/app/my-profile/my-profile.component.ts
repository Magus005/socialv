import { Component, OnInit } from '@angular/core';
import { PostsService } from '../shared/services/posts/posts.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
  currentUser: any = {};
  defaultProfile = '/assets/images/profile-user.png';
  posts: any [];
  listFollowers: any = [];
  listFollowing: any = [];

  constructor(private postService: PostsService) { }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('current_user'));
    console.log('current_user', this.currentUser);
    this.listPostsByUser();


    if (this.currentUser.listFollowers != null) {
      this.listFollowers = this.currentUser.listFollowers.toString().split(',');
    }
    if (this.currentUser.listFollowing != null) {
      this.listFollowing = this.currentUser.listFollowing.toString().split(',');
    }
  }

  listPostsByUser() {
    this.postService.findByUser(this.currentUser.id).subscribe((response: any) => {
      console.log(response);
      this.posts = response;
    });
  }

  _reloadAllCardView(event) {
    this.listPostsByUser();
  }

}
