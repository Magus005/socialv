import { Routes } from '@angular/router';
import { AuthGuardService as AuthGuard  } from '../auth/auth-guard.service';
import { MyProfileComponent } from './my-profile.component';


export const MyProfileRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: MyProfileComponent, canActivate: [AuthGuard]  }
];
