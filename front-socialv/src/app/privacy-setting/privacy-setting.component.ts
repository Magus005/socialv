import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../shared/services/users/users.service';

@Component({
  selector: 'app-privacy-setting',
  templateUrl: './privacy-setting.component.html',
  styleUrls: ['./privacy-setting.component.scss']
})
export class PrivacySettingComponent implements OnInit {
  currentUser: any = {};
  currentUserId = '';

  constructor(private userService: UsersService, private router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('current_user'));
    this.currentUserId = this.currentUser.id;
    console.log('currentUser ', this.currentUserId);
  }

  ngOnInit(): void {
  }

  cancelAccount(event) {
    event.preventDefault();
    this.userService.cancel(this.currentUserId).subscribe((response: any) => {
      console.log('response ', response);
      localStorage.removeItem('access_token');
      localStorage.removeItem('current_user');
      this.router.navigate(['login']);
    })
  }

  deleteAccount(event) {
    event.preventDefault();
    this.userService.hardDelete(this.currentUser.id).subscribe((response: any) => {
      console.log('response ', response);
      localStorage.removeItem('access_token');
      localStorage.removeItem('current_user');
      this.router.navigate(['login']);

    })
  }

}
