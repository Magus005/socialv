import { Routes } from '@angular/router';
import { AuthGuardService as AuthGuard  } from '../auth/auth-guard.service';
import { PrivacySettingComponent } from './privacy-setting.component';


export const PrivacySettingRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: PrivacySettingComponent, canActivate: [AuthGuard]  }
];
