import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, ComponentFactoryResolver, HostListener, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { CardViewImageComponent } from 'src/components/card-view-image/card-view-image.component';
import { PostsService } from '../shared/services/posts/posts.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  content: string;
  message = '';
  url: any = null;
  imagePath: any = null;

  listPosts: any = [];
  globalPosts: any = [];
  pages = 0;
  limit = 10;
  size = 0;
  theEnd = false;
  currentUser: any = {};

  defaultProfile = '/assets/images/profile-user.png';
  itsLoad = false;


  @ViewChild('cardViewInsert', { read: ViewContainerRef }) dynamicInsert: ViewContainerRef;

  constructor(@Inject(DOCUMENT) private document: Document, private postService: PostsService, private http: HttpClient,
              private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
    if (!localStorage.getItem('theme')) {
      localStorage.setItem('theme', '0');
    } else {
      this.loadCssDynamically(localStorage.getItem('theme'));
    }
    this.getAllPosts();
    this.getPostsByStep();
    this.currentUser = JSON.parse(localStorage.getItem('current_user'));
    console.log('current user', this.currentUser.id);

    window.scrollTo(0, 0);
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    //In chrome and some browser scroll is given to body tag
    const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    const max = document.documentElement.scrollHeight;
    if (pos == max )   {
      // Neant
    }
    if ((window.innerHeight + window.scrollY + 1) >= document.body.offsetHeight) {
      // you're at the bottom of the page
      if (this.theEnd == false && this.itsLoad == false) {
        this.itsLoad = true;
        const x = this.pages * this.limit;
        const y = (((this.pages + 1) * this.limit) > this.size) ? this.size : ((this.pages + 1) * this.limit);

        this.listPosts = this.listPosts.concat(this.globalPosts.slice(x, y));
        this.itsLoad = false;
        this.theEnd = (((this.pages + 1) * this.limit) > this.size) ? true : false;
      }
    }
  }


  addCardViewComponent(post) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(CardViewImageComponent);
    const dyynamicComponent = this.dynamicInsert.createComponent(componentFactory).instance;
    dyynamicComponent.postObject = post;
  }

  onFileChanged(event) {
    const files = event.target.files;
    if (files.length === 0) { return; }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
        this.message = "Only images are supported.";
        return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
        this.url = reader.result;
    };
  }

  urlify(text) {
    const urlRegex = /(https?:\/\/[^ ]*)/;
    const url = text.match(urlRegex);
    if (url == null) {
      return null;
    } else {
      return url[1];
    }
  }


  loadCssDynamically(value) {
    if (value === '0') { // light
      this.loadTheme('/assets/css/typography.css');
      this.loadTheme('/assets/css/style.css');
    } else {
      this.loadTheme('/assets/dark/css/typography.css');
      this.loadTheme('/assets/dark/css/style.css');
    }
  }

  loadTheme(cssFile: string) {
    const headEl = this.document.getElementsByTagName('head')[0];
    const newLinkEl = this.document.createElement('link');
    newLinkEl.rel = 'stylesheet';
    newLinkEl.href = cssFile;

    headEl.appendChild(newLinkEl);
  }

  initField() {
    this.content = '';
    this.url = null;
  }

  getAllPosts() {
    this.postService.findOrderBy().subscribe((response: any) => {
      console.log(response);
      this.globalPosts = response;
      this.size = response.length;
    });
  }

  getPostsByStep() {
    this.postService.findByStep(this.pages, this.limit).subscribe((response: any) => {
      console.log('postService.findByStep : ', response);
      if (this.pages == 0) {
        this.listPosts = response;
      } else {
        this.listPosts = this.listPosts.concat(response);
      }
      if (this.listPosts.length === this.size) {
        console.log('getPostsByStep this.posts.length : ', this.listPosts.length);
        console.log('getPostsByStep this.size : ', this.size);
        this.theEnd = true;
      }
      this.pages += 1;
      this.itsLoad = false;
    });
  }

  savePost(event) {
    event.preventDefault();
    const formData: any = new FormData();
    formData.append('content', this.content);
    if (this.imagePath != null) {
      formData.append('picture', this.imagePath[0]);
    }
    formData.append('userId', this.currentUser.id);
    formData.append('link', this.urlify(this.content));
    formData.append('type', 'Post');


    this.postService.create(formData).subscribe((response: {}) => {
      console.log(response);
      response['hours'] = 'Just now';
      this.addCardViewComponent(response);
    });
  }


  _reloadAllCardView(event) {
    this.getAllPosts();
    this.getPostsByStep();
  }
}
