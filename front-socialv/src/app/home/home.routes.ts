import { Routes } from '@angular/router';
import { AuthGuardService as AuthGuard  } from '../auth/auth-guard.service';
import { HomeComponent } from './home.component';


export const HomeRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: HomeComponent, canActivate: [AuthGuard]  }
];
