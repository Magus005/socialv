export const environment = {
  production: true,
  apiUri: 'https://node-api-microblogging.herokuapp.com'
};
