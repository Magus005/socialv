import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardViewUriComponent } from './card-view-uri.component';

describe('CardViewUriComponent', () => {
  let component: CardViewUriComponent;
  let fixture: ComponentFixture<CardViewUriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardViewUriComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardViewUriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
