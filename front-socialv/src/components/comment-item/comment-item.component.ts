import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-comment-item',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.scss']
})
export class CommentItemComponent implements OnInit {
  @Input('comment') comment: any = {};
  defaultProfile = '/assets/images/profile-user.png';

  constructor() { }

  ngOnInit(): void {
    console.log('commentitemcomponent commment : ', this.comment);
  }

  msToTime(ms) {
    const seconds = (ms / 1000).toFixed(1);
    const minutes = (ms / (1000 * 60)).toFixed(1);
    const hours = (ms / (1000 * 60 * 60)).toFixed(1);
    const days = (ms / (1000 * 60 * 60 * 24)).toFixed(1);
    if (Number(seconds) < 1) {
      return 'Just now';
    }
    if (Number(seconds) < 60) { return `${seconds.split('.')[0]} seconds ago`; }
    else if (Number(minutes) < 60) { return `${minutes.split('.')[0]} minutes ago`; }
    else if (Number(hours) < 24) { return `${hours.split('.')[0]} hours ago`; }
    else { return `${days.split('.')[0]} days ago`; }
  }

  getHoursComment(comment) {
    const diffInMs = new Date().valueOf() - Date.parse(comment.updatedAt);

    return this.msToTime(diffInMs);
  }


}
