import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.scss']
})
export class CardViewComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('postObject') postObject: any = {};
  postsCardViewImage = null;
  postsCardViewUri = null;
  postsCardViewText = null;


  @Output() reloadAllCardView: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
    console.log('card view postObject', this.postObject);
    this.separationListPosts(this.postObject);
  }


  // tslint:disable-next-line:typedef
  separationListPosts(post) {
    if (post.picture != '' && post.picture != null && post.picture != "null") {
      this.postsCardViewImage = post;
      console.log('separationListPosts post.picture = ', this.postsCardViewImage);
    } else if (post.link != '' && post.link != null && post.link != "null") {
      this.postsCardViewUri = post;
      console.log('separationListPosts post.link = ', this.postsCardViewUri);
    } else {
      this.postsCardViewText = post;
      console.log('separationListPosts post.text = ', this.postsCardViewText);
    }
  }



  _reloadOneCardViewImage(event) {
    this.reloadAllCardView.emit('true');
  }

  _reloadOneCardViewText(event) {
    this.reloadAllCardView.emit('true');
  }

  _reloadOneCardViewUri(event) {
    this.reloadAllCardView.emit('true');
  }

}
