import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ThemeService } from 'src/app/shared/services/theme/theme.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isThemeDark: Observable<boolean>;
  dynamicFlag = false;

  currentUser: any = {};
  defaultProfile = '/assets/images/profile-user.png';

  constructor(private themeService: ThemeService, @Inject(DOCUMENT) private document: Document, public router: Router) { }

  ngOnInit(): void {
    this.isThemeDark = this.themeService.isThemeDark;
    if (!localStorage.getItem('theme')) {
      localStorage.setItem('theme', '0');
    } else {
      this.loadCssDynamically(localStorage.getItem('theme'));
    }

    this.currentUser = JSON.parse(localStorage.getItem('current_user'));
  }

  clickDarkTheme(event) {
    event.preventDefault();

    if (localStorage.getItem('theme') === '1') {
      localStorage.setItem('theme', '0');
    } else {
      localStorage.setItem('theme', '1');
    }
    this.loadCssDynamically(localStorage.getItem('theme'));
  }

  loadCssDynamically(value) {
    if (value === '0') { // light
      this.loadTheme('/assets/css/typography.css');
      this.loadTheme('/assets/css/style.css');
    } else {
      this.loadTheme('/assets/dark/css/typography.css');
      this.loadTheme('/assets/dark/css/style.css');
    }
    this.dynamicFlag = true;
  }

  loadTheme(cssFile: string) {
    const headEl = this.document.getElementsByTagName('head')[0];
    const newLinkEl = this.document.createElement('link');
    newLinkEl.rel = 'stylesheet';
    newLinkEl.href = cssFile;

    headEl.appendChild(newLinkEl);
  }

  signUp(event) {
    event.preventDefault();
    localStorage.removeItem('access_token');
    localStorage.removeItem('current_user');
    this.router.navigate(['login']);
  }
}
