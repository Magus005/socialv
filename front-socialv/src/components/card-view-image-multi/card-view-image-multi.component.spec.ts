import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardViewImageMultiComponent } from './card-view-image-multi.component';

describe('CardViewImageMultiComponent', () => {
  let component: CardViewImageMultiComponent;
  let fixture: ComponentFixture<CardViewImageMultiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardViewImageMultiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardViewImageMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
