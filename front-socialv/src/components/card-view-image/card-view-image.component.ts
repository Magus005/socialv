import { HttpClient } from '@angular/common/http';
import { Component, ComponentFactoryResolver, EventEmitter, Input, OnInit, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { CommentsService } from 'src/app/shared/services/comments/comments.service';
import { LikesService } from 'src/app/shared/services/likes/likes.service';
import { PostsService } from 'src/app/shared/services/posts/posts.service';
import { UsersService } from 'src/app/shared/services/users/users.service';
import { CommentItemComponent } from '../comment-item/comment-item.component';

@Component({
  selector: 'app-card-view-image',
  templateUrl: './card-view-image.component.html',
  styleUrls: ['./card-view-image.component.scss']
})
export class CardViewImageComponent implements OnInit {
  @Input('postObject') postObject: any = {};
  userOfThisPost: any = {};
  listLike: any = [];
  listComments: any = [];
  currentLike: any = {};
  currentUser: any = {};

  content: string = 'fdlsqfjds';
  comment: string;
  message = '';
  url: any = null;
  imagePath: any;
  defaultProfile = '/assets/images/profile-user.png';
  listFollowers: any = [];
  listFollowingUserOfThisPost: any = [];

  userIdOfThisPost: string;
  currentUserId: string;
  numberOfLikes = 0;

  text = '';
  links;
  isLoading = false;

  @Output() reloadAllCardView: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('commentItemInsert', { read: ViewContainerRef }) dynamicInsertComment: ViewContainerRef;


  constructor(private userService: UsersService, private http: HttpClient,
              private likeService: LikesService, private postService: PostsService,
              private commentService: CommentsService, private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {
    this.getUserById();
    console.log('postObject', this.postObject);
    const diffInMs = new Date().valueOf() - Date.parse(this.postObject.updatedAt);

    this.postObject.hours = this.msToTime(diffInMs);

    this.currentUser = JSON.parse(localStorage.getItem('current_user'));
    console.log('current user', this.currentUser);


    this.listLikeByPost();
    this.listCommentByPost();
    this.listLikeByPostAndUser();

    this.content = this.postObject.content;
    this.url = this.postObject.picture;
    console.log('current post url', this.url);
    console.log('current post content', this.content);


    if (this.currentUser.listFollowers != null) {
      this.listFollowers = this.currentUser.listFollowers.toString().split(',');
    }

    this.url = this.postObject.link;
    if (this.url) {
      this.getDetailUri(this.url);
    }

    this.userIdOfThisPost = this.postObject.userId.toString();
    this.currentUserId = this.currentUser.id.toString();
    console.log('list followers [] ', this.listFollowers);
    console.log('list followers [] ', this.listFollowers.includes(this.userIdOfThisPost));
  }

  getDetailUri(uri) {
    this.http.get(
      `http://api.linkpreview.net/?key=79c58307f54d544b78d726c26b00a169&q=${uri}`)
    .subscribe(res => {
      this.links = res;
      console.log('links = ', this.links);
      this.isLoading = true;
    });

  }

  openUri(event) {
    event.preventDefault();
    window.open(this.links.url, "_blank");
  }
  convertStringToArray(value) {
    if (value == null) {
      return [];
    } else {
      return value.split(',');
    }
  }

  msToTime(ms) {
    const seconds = (ms / 1000).toFixed(1);
    const minutes = (ms / (1000 * 60)).toFixed(1);
    const hours = (ms / (1000 * 60 * 60)).toFixed(1);
    const days = (ms / (1000 * 60 * 60 * 24)).toFixed(1);
    if(Number(seconds) < 1) {
      return 'Just now';
    }
    if (Number(seconds) < 60) { return `${seconds.split('.')[0]} seconds ago`; }
    else if (Number(minutes) < 60) { return `${minutes.split('.')[0]} minutes ago`; }
    else if (Number(hours) < 24) { return `${hours.split('.')[0]} hours ago`; }
    else { return `${days.split('.')[0]} days ago`; }
  }

  getUserById() {
    this.userService.findById(this.postObject.userId).subscribe((response: {}) => {
      console.log(response);
      this.userOfThisPost = response;
      console.log('userOfThisPost', this.userOfThisPost);

      if (this.userOfThisPost.listFollowing != null) {
        this.listFollowingUserOfThisPost = this.userOfThisPost.listFollowing.toString().split(',');
      }
      console.log('list followers [] ', this.userIdOfThisPost);
    }, (error) => {
      console.log('error: ', error);
    })
  }

  saveLike(event) {
    event.preventDefault();
    console.log('this.currentLike : ', this.currentLike);
    if (this.currentLike == null) {
      const like = {
        userId: this.currentUser.id,
        postId: this.postObject.id,
        like: '1'
      };
      this.likeService.create(like).subscribe((response: {}) => {
        this.listLikeByPost();
        this.listLikeByPostAndUser();
      });
    } else {
      this.updateLike();
    }
  }

  updateLike() {
    const like = {
      like: this.currentLike.like == '1' ? '0' : '1'
    };
    this.likeService.update(like, this.currentLike.id).subscribe((response: {}) => {
      this.listLikeByPost();
      this.listLikeByPostAndUser();
    });
  }

  listLikeByPost() {
    this.likeService.findByPost(this.postObject.id).subscribe((response: {}) => {
      console.log('listLike', response);
      this.listLike = response;
      this.numberOfLikes = this.likeNumberCalculation(response);
    }, (error) => {
      console.error('error', error);
    });
  }

  listLikeByPostAndUser() {
    this.likeService.findByPostAndUser(this.currentUser.id, this.postObject.id).subscribe((response: {}) => {
      this.currentLike = response;
      console.log('this.currentLike 1: ', this.currentLike);
      console.log('this.currentLike 1: this.currentUser.id', this.currentUser.id);
      console.log('this.currentLike 1: this.postObject.id', this.postObject.id);
    });
  }

  likeNumberCalculation(likes) {
    let n = 0;
    console.log('likeNumberCalculation likes ', likes);
    likes.forEach(like => {
      if (like.like == '1') {
        n += 1;
        console.log('likeNumberCalculation n ', n);
      }
    });
    return n;
  }

  onFileChanged(event) {
    const files = event.target.files;
    if (files.length === 0) { return; }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
        this.message = "Only images are supported.";
        return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
        this.url = reader.result;
    };
  }

  urlify(text) {
    const urlRegex = /(https?:\/\/[^ ]*)/;
    const url = text.match(urlRegex);
    if (url == null) {
      return null;
    } else {
      return url[1];
    }
  }


  updatePost(event) {
    event.preventDefault();
    const postUpdate = this.createObjectUpdatePost();

    console.log('post update', postUpdate);

    this.postService.update(postUpdate, this.postObject.id).subscribe((response: {}) => {
      console.log(response);
      this.reloadAllCardView.emit('true');
      this.content = '';
    });

    console.log('content', this.content);
  }

  createObjectUpdatePost() {
    var postUpdate: any = new FormData();
    if (this.imagePath) {
      postUpdate.append('content', this.content);
      console.log('imagePath', this.imagePath);
      postUpdate.append('picture', this.imagePath[0]);
      postUpdate.append('link', this.urlify(this.content));

      console.log('formData', postUpdate);
      return postUpdate;
    } else {
      return {
        content: this.content,
        link: this.urlify(this.content)
      }
    }
  }

  deleteThisPost(event) {
    event.preventDefault();

    this.postService.remove(this.postObject.id).subscribe((response: {}) => {
      console.log('response : ', response);
    });
    this.reloadAllCardView.emit('true');
  }

  followThisUser(event) {
    event.preventDefault();

    this.listFollowers.push(this.userOfThisPost.id);
    this.listFollowingUserOfThisPost.push(this.currentUser.id);

    console.log('verif ', this.listFollowers.includes(this.userIdOfThisPost));

    console.log('list followers follow : ', this.listFollowers);
    console.log('list following follow : ', this.listFollowingUserOfThisPost);

    this.userService.update({listFollowers: this.listFollowers.toString()}, this.currentUser.id).subscribe((response: {}) => {
      console.log('response : ', response);
      this.currentUser.listFollowers = this.listFollowers.toString();
      localStorage.setItem('current_user', JSON.stringify(this.currentUser));
      this.userService.update({listFollowing: this.listFollowingUserOfThisPost.toString()},
                              this.postObject.userId).subscribe((response: {}) => {
        console.log('response : ', response);
        this.reloadAllCardView.emit('true');
      });
    });
  }

  unfollowThisUser(event) {
    event.preventDefault();
    const indexFollower = this.listFollowers.indexOf(this.userIdOfThisPost);
    const indexFollowing = this.listFollowingUserOfThisPost.indexOf(this.currentUserId);

    if (indexFollower > -1) {
      this.listFollowers.splice(indexFollower, 1);
      console.log('list followers unfollow : ', this.listFollowers);
    }
    if (indexFollowing > -1) {
      this.listFollowingUserOfThisPost.splice(indexFollowing, 1);
      console.log('list following unfollow : ', this.listFollowingUserOfThisPost);
    }

    this.userService.update({listFollowers: this.listFollowers.toString()}, this.currentUser.id).subscribe((response: {}) => {
      console.log('response : ', response);
      this.currentUser.listFollowers = this.listFollowers.toString();
      localStorage.setItem('current_user', JSON.stringify(this.currentUser));
      this.userService.update({listFollowing: this.listFollowingUserOfThisPost.toString()},
                              this.postObject.userId).subscribe((response: {}) => {
        console.log('response : ', response);
        this.reloadAllCardView.emit('true');
      });
    });
  }



  listCommentByPost() {
    this.commentService.findByPost(this.postObject.id).subscribe((response: {}) => {
      console.log('list Comments', response);
      this.listComments = response;
    }, (error) => {
      console.error('error', error);
    });
  }

  getHoursComment(comment) {
    const diffInMs = new Date().valueOf() - Date.parse(comment.updatedAt);

    return this.msToTime(diffInMs);
  }

  saveComment(event) {
    const comment = {
      content: this.comment,
      userId: this.currentUser.id,
      postId: this.postObject.id
    };
    this.commentService.create(comment).subscribe((response: {}) => {
      console.log('save comment response : ', response);
      this.addCardViewComponent({comment: response, user: this.currentUser});
      this.listCommentByPost();
      this.comment = '';
    });
  }



  addCardViewComponent(comment) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(CommentItemComponent);
    const dyynamicComponent = this.dynamicInsertComment.createComponent(componentFactory).instance;
    dyynamicComponent.comment = comment;
  }

}
