import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardViewImageComponent } from './card-view-image.component';

describe('CardViewImageComponent', () => {
  let component: CardViewImageComponent;
  let fixture: ComponentFixture<CardViewImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardViewImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardViewImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
